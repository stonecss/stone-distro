core = 7.x
api = 2

; Drupal Core
projects[drupal][version] = 7.34
projects[drupal][patch][2033883] = http://drupal.org/files/orderedResults.patch

; Stone Profile
projects[stone_profile][type] = profile
projects[stone_profile][download][type] = git
projects[stone_profile][download][url] = git@bitbucket.org:stonecss/stone-profile.git
projects[stone_profile][download][branch] = 7.x-1.x
